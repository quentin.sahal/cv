import type { VercelRequest, VercelResponse } from "@vercel/node";

export default function handler(
  request: VercelRequest,
  response: VercelResponse
) {
  console.log({ ip: request.headers });
  response.status(200).json({ ok: true });
}
